# CreNum



## Installation

**Déploiement en local**

`cd /var/www/html`

`git clone git@gitlab.com:litt-arts-num/crenum.git`

`composer install`

La visualisation est disponible à l'URL `localhost/crenum`.

## Contenu

Le site est construit avec un squelette `twig` (`/templates`) dans lequel on injecte les sorties de fichiers `xsl` (`/templates/data`) qui permettent d'adapter les données (`/templates/data/xml`) à la visualisation.


## Mise à jour des données
Mettre à jour les fichiers sources (XML-TEI) dans le dossier data/xml puis effectuer les transformations suivantes :
```sh
# Création d'une version restructurée par page
saxonb-xslt data/xml/livreXXX.xml data/xslt/split_pages.xsl >data/xml/livreXXX_split.xml
# Création des sorties html
saxonb-xslt data/xml/livreXXX_split.xml data/xslt/xslt_livre.xsl >templates/livreXXX.htm
```
