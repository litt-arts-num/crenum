//récupérer le premier identifiant de page (ne commence pas toujours à 1)
var pages = document.getElementsByClassName("page");
var begId = pages[0].id.split("-")[1];
var endId = pages[pages.length-1].id.split("-")[1];
var minPageNb = Number(begId.substring(1,begId.length));
var maxPageNb = Number(endId.substring(1,endId.length));
var currentPageNb = minPageNb;

window.onload = function(){
  /*fonctionne -> mais fair eun onclick au bouton a traiter car sinon tous mis en actif
    document.querySelectorAll('.btn').forEach(buttonElement => {
    const button = bootstrap.Button.getOrCreateInstance(buttonElement);
    button.toggle();
  })*/

  afficherPage(currentPageNb);
}

//récupération et masquage des éléments correspondants à pageNb
function cacherPage(pageNb){
  document.getElementById("page-f"+pageNb+"-left").style.display="none";
  document.getElementById("page-f"+pageNb+"-right").style.display="none";
  document.getElementById("diplo-f"+pageNb+"-left").style.display="none";
  document.getElementById("diplo-f"+pageNb+"-right").style.display="none";
  document.getElementById("linear-f"+pageNb+"-left").style.display="none";
  document.getElementById("linear-f"+pageNb+"-right").style.display="none";
}

//récupération et affichage des éléments correspondants à pageNb
function afficherPage(pageNb){
  //alert(document.location.href);
    //href A EVITER (se concatène trop) - peut marcher si regle
    //document.location.href="?page=livre1&num="+currentPageNb;
    //ajoute le numero de page dans l'url (http://localhost/crenum/index.php?page=livrenumPage)
    //window.location = url+"&num="+currentPageNb;
    //ajoute le numero de page dans l'url (http://localhost/crenum/index.php?page=livre1&num=pageNb) pb : boucle infinie
  //window.location.href += "&num="+pageNb;
  document.getElementById("page-f"+pageNb+"-left").style.display="block";
  document.getElementById("page-f"+pageNb+"-right").style.display="block";
  document.getElementById("diplo-f"+pageNb+"-left").style.display="block";
  document.getElementById("diplo-f"+pageNb+"-right").style.display="block";
  document.getElementById("linear-f"+pageNb+"-left").style.display="block";
  document.getElementById("linear-f"+pageNb+"-right").style.display="block";
}

//masquer la page actuelle et afficher la page précédente (s'il y en a une)
function prev(){
  if(currentPageNb > minPageNb){
    cacherPage(currentPageNb);
    currentPageNb--;
    afficherPage(currentPageNb);
  } else{
    alert("Pas de page précédente");
  }
}

//masquer la page actuelle et afficher la page suivante (s'il y en a une)
function next(){
  if(currentPageNb < maxPageNb){
    cacherPage(currentPageNb);
    currentPageNb++;
    afficherPage(currentPageNb);
  } else{
    alert("Pas de page suivante");
  }
}

//changement de page au click sur les touches flèches gauche et droite
function touch(event){
	var key = window.event ? event.keyCode : event.which;
  if(key == 37){
    prev();
  }
  else if(key==39){
    next();
  }
}
