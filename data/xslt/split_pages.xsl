<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
  xmlns:tei="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="tei"
  xmlns="http://www.tei-c.org/ns/1.0">

  <xsl:output encoding="utf-8" indent="no" method="xml" exclude-result-prefixes="tei"/>

  <xsl:template match="* | @*" mode="#default">
    <xsl:copy copy-namespaces="no">
      <xsl:apply-templates select="@*|node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="tei:front" mode="#default">
    <xsl:variable name="context" select="." as="element(tei:front)"/>
    <xsl:copy>
      <xsl:for-each-group select="descendant::node()[not(node())]" group-starting-with="tei:pb">
        <xsl:if test="position() > 1">
          <div>
            <xsl:attribute name="type">page</xsl:attribute>
            <xsl:copy-of select="self::tei:pb"/>
            <xsl:apply-templates select="$context/*" mode="split">
              <xsl:with-param name="restricted-to" select="current-group()/ancestor-or-self::node()"
                tunnel="yes"/>
            </xsl:apply-templates>
          </div>
        </xsl:if>
      </xsl:for-each-group>
    </xsl:copy>
  </xsl:template>

  <!-- on match le <div type="book"> et pas le <body> car tout est dans le premier-->
  <xsl:template match="tei:div[@type='book']" mode="#default">
    <xsl:variable name="context" select="." as="element(tei:div)"/>
      <xsl:copy>
        <xsl:for-each-group select="descendant::node()[not(node())]" group-starting-with="tei:pb">
          <xsl:if test="position() > 1">
            <div>
              <xsl:attribute name="type">page</xsl:attribute>
              <xsl:copy-of select="self::tei:pb"/>
              <xsl:apply-templates select="$context/*" mode="split">
                <xsl:with-param name="restricted-to" select="current-group()/ancestor-or-self::node()"
                  tunnel="yes"/>
              </xsl:apply-templates>
            </div>
          </xsl:if>
        </xsl:for-each-group>
      </xsl:copy>
  </xsl:template>

  <xsl:template match="node()" mode="split">
    <xsl:param name="restricted-to" as="node()+" tunnel="yes"/>
    <xsl:if test="exists(. intersect $restricted-to)">
      <xsl:copy>
        <xsl:copy-of select="@*"/>
        <xsl:apply-templates mode="#current"/>
      </xsl:copy>
    </xsl:if>
  </xsl:template>

  <xsl:template match="tei:pb" mode="split"/>

  <xsl:template match="text()" mode="split">
    <xsl:param name="restricted-to" as="node()+" tunnel="yes"/>
    <xsl:if test="exists(. intersect $restricted-to)">
      <xsl:value-of select='replace(.,"([:;!?])","&amp;#160;$1")' disable-output-escaping="yes"/>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
