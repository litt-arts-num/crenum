<?xml version="1.0" encoding="UTF-8"?>
<!-- XSLT - Méthode 1 : Élément par élément
    Exemple applicable tel quel sur des fichiers TEI, prévu pour générer un fichier HTML
    incluant un appel à Bootstap (bibliothèque CSS permettant des facilités d'affichage).

    Voir : https://getbootstrap.com/docs/5.0/getting-started/introduction/
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>
    <xsl:variable name="img_width" select="700"/>
    <xsl:strip-space elements="*"/>
    <!--<xsl:preserve-space elements="tei:ab tei:bibl tei:note tei:title"/>-->

    <!-- Template qui s'applique à la racine du XML -->
    <xsl:template match="/">

        <div class="col-5">
            <nav>
                <div class="nav nav-tabs" id="nav-tab-left" role="tablist">
                    <button class="nav-link active" id="page-left-tab" data-bs-toggle="tab"
                        data-bs-target="#page-left" type="button" role="tab"
                        aria-controls="page-left" aria-selected="true">Manuscrit</button>
                    <button class="nav-link" id="diplo-left-tab" data-bs-toggle="tab"
                        data-bs-target="#diplo-left" type="button" role="tab"
                        aria-controls="diplo-left" aria-selected="false">Transcription
                        diplomatique</button>
                    <button class="nav-link" id="linear-left-tab" data-bs-toggle="tab"
                        data-bs-target="#linear-left" type="button" role="tab"
                        aria-controls="linear-left" aria-selected="false">Transcription
                        semi-modernisée</button>
                </div>
            </nav>
            <br/>

            <div class="tab-content" id="nav-tabContent-left">
                <div class="tab-pane fade show active" id="page-left" role="tabpanel"
                    aria-labelledby="page-left-tab" tabindex="0">
                    <!-- Récupérer l'img lien Gallica -->
                    <xsl:for-each select="/tei:TEI/tei:text//tei:div[@type = 'page']">
                        <xsl:variable name="id-left"
                            select="concat(substring-before(substring-after(tei:pb/@facs, 'btv1b105206468/'), '.item'), '-left')"/>
                        <div class="page" id="page-{$id-left}" style="display:none">
                            <xsl:apply-templates mode="img" select="tei:pb"/>
                        </div>
                    </xsl:for-each>
                </div>
                <div class="tab-pane fade" id="diplo-left" role="tabpanel"
                    aria-labelledby="diplo-left-tab" tabindex="0">
                    <!-- Récupérer la version diplomatique -->
                    <xsl:for-each select="/tei:TEI/tei:text//tei:div[@type = 'page']">
                        <xsl:variable name="id-left"
                            select="concat(substring-before(substring-after(tei:pb/@facs, 'btv1b105206468/'), '.item'), '-left')"/>
                        <div id="diplo-{$id-left}" style="display:none">
                            <xsl:apply-templates mode="diplo"/>
                        </div>
                    </xsl:for-each>
                </div>
                <div class="tab-pane fade" id="linear-left" role="tabpanel"
                    aria-labelledby="linear-left-tab" tabindex="0">
                    <!-- Récupérer la version semi-modernisée -->
                    <xsl:for-each select="/tei:TEI/tei:text//tei:div[@type = 'page']">
                        <xsl:variable name="id-left"
                            select="concat(substring-before(substring-after(tei:pb/@facs, 'btv1b105206468/'), '.item'), '-left')"/>
                        <div id="linear-{$id-left}" style="display:none">
                            <xsl:apply-templates mode="linear"/>
                        </div>
                    </xsl:for-each>
                </div>
            </div>
        </div>

        <div class="col-5">
            <nav>
                <div class="nav nav-tabs" id="nav-tab-right" role="tablist">
                    <button class="nav-link" id="page-right-tab" data-bs-toggle="tab"
                        data-bs-target="#page-right" type="button" role="tab"
                        aria-controls="page-right" aria-selected="false">Manuscrit</button>
                    <button class="nav-link active" id="diplo-right-tab" data-bs-toggle="tab"
                        data-bs-target="#diplo-right" type="button" role="tab"
                        aria-controls="diplo-right" aria-selected="true">Transcription
                        diplomatique</button>
                    <button class="nav-link" id="linear-right-tab" data-bs-toggle="tab"
                        data-bs-target="#linear-right" type="button" role="tab"
                        aria-controls="linear-right" aria-selected="false">Transcription
                        semi-modernisée</button>
                </div>
            </nav>
            <br/>

            <div class="tab-content" id="nav-tabContent-right">
                <div class="tab-pane fade" id="page-right" role="tabpanel"
                    aria-labelledby="page-right-tab" tabindex="0">
                    <!-- Récupérer l'img lien Gallica -->
                    <xsl:for-each select="/tei:TEI/tei:text//tei:div[@type = 'page']">
                        <xsl:variable name="id-right"
                            select="concat(substring-before(substring-after(tei:pb/@facs, 'btv1b105206468/'), '.item'), '-right')"/>
                        <div class="page" id="page-{$id-right}" style="display:none">
                            <xsl:apply-templates mode="img" select="tei:pb"/>
                        </div>
                    </xsl:for-each>
                </div>
                <div class="tab-pane fade show active" id="diplo-right" role="tabpanel"
                    aria-labelledby="diplo-right-tab" tabindex="0">
                    <!-- Récupérer la version diplomatique -->
                    <xsl:for-each select="/tei:TEI/tei:text//tei:div[@type = 'page']">
                        <xsl:variable name="id-right"
                            select="concat(substring-before(substring-after(tei:pb/@facs, 'btv1b105206468/'), '.item'), '-right')"/>
                        <div id="diplo-{$id-right}" style="display:none">
                            <xsl:apply-templates mode="diplo"/>
                        </div>
                    </xsl:for-each>
                </div>
                <div class="tab-pane fade" id="linear-right" role="tabpanel"
                    aria-labelledby="linear-right-tab" tabindex="0">
                    <!-- Récupérer la version semi-modernisée -->
                    <xsl:for-each select="/tei:TEI/tei:text//tei:div[@type = 'page']">
                        <xsl:variable name="id-right"
                            select="concat(substring-before(substring-after(tei:pb/@facs, 'btv1b105206468/'), '.item'), '-right')"/>
                        <div id="linear-{$id-right}" style="display:none">
                            <xsl:apply-templates mode="linear"/>
                        </div>
                    </xsl:for-each>
                </div>
            </div>
        </div>
    </xsl:template>

    <!-- Template autofermant qui permet, pour un élément donné, de ne rien faire -->
    <xsl:template match="tei:teiHeader"/>

    <!-- On ignore les pb sauf en mode img où pour chaque changement de page, on affiche l'image correspondante -->
    <xsl:template match="tei:pb" mode="diplo linear"/>
    <xsl:template match="tei:pb" mode="img">
        <img class="img-fluid" loading="lazy">
            <xsl:attribute name="id">
                <xsl:value-of
                    select="substring-before(substring-after(./@facs, 'btv1b105206468/'), '.item')"
                />
            </xsl:attribute>
            <xsl:attribute name="src">
                <xsl:value-of
                    select="concat(substring-before(./@facs, 'ark:'), 'iiif/', substring-before(substring-after(./@facs, '.fr/'), '.item'), '/full/', $img_width, ',/0/native.jpg')"
                />
            </xsl:attribute>
        </img>
    </xsl:template>

    <!-- On ignore les fw sauf en pseudo-diplo -->
    <xsl:template match="tei:fw" mode="linear"/>
    <xsl:template match="tei:fw" mode="diplo">
        <p title="Numéro de {@type}" class="text-end">
            <i>
                <xsl:value-of select="@n"/>
            </i>
        </p>
    </xsl:template>

    <xsl:template match="tei:p" mode="#all">
        <p>
            <xsl:apply-templates mode="#current"/>
        </p>
    </xsl:template>
    <xsl:template match="tei:div" mode="#all">
        <div>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>

    <xsl:template match="tei:title" mode="diplo">
        <span class="title">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:title" mode="linear">
        <i title="Titre">
            <xsl:apply-templates mode="#current"/>
        </i>
    </xsl:template>

    <xsl:template match="tei:choice" mode="diplo">
        <xsl:choose>
            <xsl:when test="tei:abbr or tei:expan">
                <span class="choice">
                    <span title="Abbréviation pour {tei:expan}">
                        <xsl:apply-templates select="tei:abbr" mode="#current"/>
                    </span>
                </span>
            </xsl:when>
            <xsl:when test="tei:orig or tei:reg">
                <xsl:apply-templates mode="#current"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:choice" mode="linear">
        <xsl:choose>
            <xsl:when test="tei:abbr or tei:expan">
                <span class="choice">
                    <span title="Abbrégé en {tei:abbr}">
                        <xsl:apply-templates select="tei:expan" mode="#current"/>
                    </span>
                </span>
            </xsl:when>
            <xsl:when test="tei:orig or tei:reg">
                <xsl:apply-templates mode="#current"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:orig" mode="diplo">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:orig" mode="linear"/>
    <xsl:template match="tei:reg" mode="diplo"/>
    <xsl:template match="tei:reg" mode="linear">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="tei:abbr" mode="diplo">
        <mark title="abbréviation">
            <xsl:apply-templates mode="#current"/>
        </mark>
    </xsl:template>
    <xsl:template match="tei:expan" mode="linear">
        <mark title="abbréviation">
            <xsl:apply-templates mode="#current"/>
        </mark>
    </xsl:template>
    <xsl:template match="tei:ex" mode="linear">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:am" mode="diplo">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="tei:subst" mode="diplo">
        <span title="substitution">
            <span class="align-top" title="Ajout">
                <xsl:apply-templates mode="#current" select="tei:add"/>
            </span>
            <s class="align-bottom" title="Suppression">
                <xsl:apply-templates mode="#current" select="tei:del"/>
            </s>
            <xsl:if test="not(tei:add) or not(tei:del)">
                <xsl:message>Substitution étrange : add ou del seul (contexte : <xsl:value-of
                        select="parent::*"/>)</xsl:message>
            </xsl:if>
        </span>
    </xsl:template>
    <xsl:template match="tei:subst" mode="linear">
        <span title="Texte résultant d'une substitution">
            <xsl:apply-templates mode="#current" select="tei:add"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:add" mode="diplo linear">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:del" mode="diplo">
        <s>
            <xsl:apply-templates mode="#current"/>
        </s>
    </xsl:template>

    <xsl:template match="tei:app[tei:lem]" mode="diplo">
        <span title="Apparat critique encodé" class="border border-info">
            <xsl:apply-templates mode="diplo"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:lem" mode="diplo">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:rdg" mode="diplo">
        <sup>
            <xsl:text>[lecture </xsl:text>
            <xsl:value-of select="@wit"/>
            <xsl:text> : </xsl:text>
            <xsl:apply-templates mode="#current"/>
            <xsl:text>]</xsl:text>
        </sup>
    </xsl:template>

    <xsl:template match="tei:app[tei:lem]" mode="linear">
        <span title="Apparat critique encodé">
            <xsl:apply-templates mode="diplo" select="tei:lem"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:rdg" mode="linear"/>

    <xsl:template match="tei:metamark" mode="linear diplo">
        <mark title="{@rend}">
            <xsl:apply-templates mode="#current"/>
        </mark>
    </xsl:template>

    <xsl:template match="tei:cit" mode="diplo">
        <span title="Citation">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:cit" mode="linear">
        <span title="Citation">
            <xsl:text>« </xsl:text>
            <xsl:apply-templates mode="#current"/>
            <xsl:text> »</xsl:text>
        </span>
    </xsl:template>

    <xsl:template match="tei:hi[@rend = 'smallcaps']" mode="#all">
        <span style="font-variant:small-caps;">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'superscript']" mode="#all">
        <sup>
            <xsl:apply-templates mode="#current"/>
        </sup>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'italic']" mode="#all">
        <i>
            <xsl:apply-templates mode="#current"/>
        </i>
    </xsl:template>

    <xsl:template match="tei:lg" mode="#all">
        <div class="lg m-2">
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:l" mode="#all">
        <span class="d-block">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:head" mode="#all">
        <blockquote class="ms-2">
            <xsl:apply-templates mode="#current"/>
        </blockquote>
    </xsl:template>

    <xsl:template match="tei:said" mode="diplo">
        <span title="Discours direct">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:said" mode="linear">
        <span title="Discours direct">
            <xsl:text>« </xsl:text>
            <xsl:apply-templates mode="#current"/>
            <xsl:text> »</xsl:text>
        </span>
    </xsl:template>

    <xsl:template match="tei:foreign" mode="#all">
        <mark title="Emprunt">
            <xsl:apply-templates mode="#current"/>
        </mark>
    </xsl:template>

    <xsl:template match="tei:anchor" mode="#all">
        <a id="{@xml:id}">
            <i class="fas fa-anchor" id="{@xml:id}"/>
        </a>
    </xsl:template>

    <xsl:template match="tei:lb" mode="diplo">
        <br/>
    </xsl:template>
    <xsl:template match="tei:lb" mode="linear"/>


    <!-- À AFFINER -->
    <xsl:template
        match="tei:gap | tei:num | tei:unclear | tei:date | tei:quote | tei:note | tei:app | tei:addSpan | tei:damage"
        mode="#all">
        <span class="{name()}">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>

    <xsl:template match="tei:geogName | tei:orgName | tei:persName | tei:placeName" mode="#all">
        <mark title="{name()}">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="#current"/>
        </mark>
    </xsl:template>

    <xsl:template match="text()" mode="diplo">
        <xsl:variable name="s">
            <xsl:choose>
                <xsl:when
                    test="not(parent::tei:c) and not(parent::tei:orig) and not(parent::tei:reg)">
                    <xsl:value-of
                        select="replace(replace(replace(replace(., 's', 'ſ'), 'ſ\b', 's', '!'), 'S’', 'ſ’'), 's’', 'ſ’')"
                    />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="j_min">
            <xsl:value-of select="replace($s, 'j', 'i')"/>
        </xsl:variable>
        <xsl:variable name="j_maj">
            <xsl:value-of select="replace($j_min, 'J', 'I')"/>
        </xsl:variable>
        <xsl:variable name="r">
            <xsl:value-of select="replace(replace($j_maj, 'rr', 'ꝛr'), '([bdghopy])r', '$1ꝛ')"/>
        </xsl:variable>
        <xsl:variable name="v">
            <xsl:choose>
                <xsl:when test="not(preceding-sibling::*[1]/name() = 'choice')">
                    <xsl:value-of select="replace(replace($r, 'v', 'u'), '\bu', 'v', '!')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="replace($r, 'v', 'u')"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="$v"/>
    </xsl:template>

    <!-- DEBUG SECTION - Templates used to show warnings if unexpected elements or attributes -->
    <xsl:template match="tei:*" mode="#all">
        <span class="btn-warning btn-sm" title="Élément non pris en charge par la XSLT.">
            <xsl:value-of select="name(.)"/>
        </span>
        <xsl:message>
            <xsl:text>Élement non pris en charge par la XSLT :</xsl:text>
            <xsl:copy/>
            <xsl:copy-of select="@*"/>
        </xsl:message>
    </xsl:template>
    <!-- END DEBUG SECTION -->
</xsl:stylesheet>
