<?php
  require_once 'vendor/autoload.php';
  $loader = new \Twig\Loader\FilesystemLoader('templates');
  $twig = new \Twig\Environment($loader);
  $page = (isset($_GET["page"])) ? $_GET["page"] : "index";
  echo $twig->render($page . '.html.twig');
?>
